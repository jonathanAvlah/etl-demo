from etl_demo import steps
from etl_demo.utils import Utils


def initialize():
    utils = Utils()
    utils.initialize()
    utils.get_resource_copy('2022_forbes_billionaires.csv')


if __name__ == '__main__':
    initialize()
    steps.run()
