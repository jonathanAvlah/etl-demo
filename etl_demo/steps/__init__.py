from .step_0 import Step0
from .step_1 import Step1
from .step_2 import Step2


def run():
    data_0 = Step0().run()
    Step1(data_0).run()
    Step2().run()
