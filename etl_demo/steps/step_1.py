from .step import Step
from datetime import datetime
import pycountry_convert as pc


class Step1(Step):
    """
    Making core transformations
    """

    def run(self):
        data = self.__worth_to_pure_digit(self.pre_data)
        data = self.__age_to_year(data)
        data = self.__country_to_continent(data)
        self.utils.export_data(data, self.data_name())

        return data

    @staticmethod
    def __worth_to_pure_digit(data):
        def transform(worth):
            digit = worth.replace('$', '').replace('B', '').strip()
            return float(digit) * 1000000000

        data['Networth'] = data.apply(lambda row: transform(row['Networth']), axis=1)
        return data

    def __age_to_year(self, data):
        data = self.utils.rename_columns(data, {'Age': 'Year'})
        data['Year'] = datetime.now().year - data['Year']
        return data

    @staticmethod
    def __country_to_continent(data):
        data['Country'] = data.apply(
            lambda row: 'Eswatini' if 'Swaziland' in row['Country'] else row['Country'],
            axis=1
        )
        data['Continent'] = data.apply(
            lambda row: pc.country_alpha2_to_continent_code(
                pc.country_name_to_country_alpha2(row['Country'], cn_name_format="default")
            ),
            axis=1
        )

        return data
