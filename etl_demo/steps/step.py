from etl_demo.utils import Utils


class Step:
    def __init__(self, pre_data=None):
        self.pre_data = pre_data
        self.utils = Utils()

    def run(self):
        pass

    def data_name(self):
        return self.__class__.__name__
