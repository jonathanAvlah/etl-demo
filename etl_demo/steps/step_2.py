from .step import Step
from unidecode import unidecode


class Step2(Step):
    """
    Making last transformations
    """

    def run(self):
        data = self.utils.read_file('Step1.csv')
        data = self.__transform_special_chars(data)
        self.utils.write_file('Forbes_ETL.csv', data)

    @staticmethod
    def __transform_special_chars(data):
        data = unidecode(data)
        data = data.replace('&', 'and')

        return data
