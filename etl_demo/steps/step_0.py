from .step import Step


class Step0(Step):
    """
    Cleaning data
    """

    def run(self):
        data = self.utils.import_data('2022_forbes_billionaires')
        data.drop(data.columns[data.columns.str.contains('unnamed', case=False)], axis=1, inplace=True)
        data = self.__capitalize_columns(data)
        data = self.__trim_data(data)

        return data

    def __capitalize_columns(self, data):
        cols = self.utils.data_columns(data)
        col_dict = {}
        for col in cols:
            col_dict[col] = col.capitalize()
        return data.rename(col_dict, axis=1)

    def __trim_data(self, data):
        cols = self.utils.data_columns(data)
        cols.pop(0)  # Not Rank
        cols.pop(2)  # Not Age
        data[cols] = data.apply(lambda row: [row[col].strip() for col in cols], axis=1).tolist()
        return data


