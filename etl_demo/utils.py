import os
from pathlib import Path
import pandas as pd
import shutil


class Utils:
    io_data_path = ''

    def __init__(self):
        Utils.io_data_path = Path('data')

    def initialize(self):
        self.create_folder(Utils.io_data_path)

    @staticmethod
    def get_working_folder():
        return Utils.io_data_path

    def read_file(self, filename):
        with open(self.get_working_folder() / filename) as f:
            return f.read()

    def write_file(self, filename, content=''):
        with open(self.get_working_folder() / filename, 'w') as f:
            print('jojo', self.get_working_folder() / filename)
            f.write(content)

    def import_data(self, filename, sep=',', data_type={}):
        return pd.read_csv(self.get_working_folder() / f'{filename}.csv', sep=sep, dtype=data_type, encoding='utf-8')

    def export_data(self, data: pd.DataFrame, filename, sep=','):
        data.to_csv(self.get_working_folder() / f'{filename}.csv', sep=sep, index=False, encoding='utf-8')

    def get_resource_copy(self, filename):
        shutil.copy(self.get_working_folder() / f'../resources/{filename}', self.get_working_folder() / filename)

    @staticmethod
    def create_folder(path):
        if not os.path.exists(path):
            os.makedirs(path)

    @staticmethod
    def data_columns(data):
        return data.columns.tolist()

    @staticmethod
    def rename_columns(data, cols, inplace=False):
        return data.rename(columns=cols, inplace=inplace)

    @staticmethod
    def data_stats(data, lines=5):
        print(data.head(lines))
        print(data.shape)
        print(data.dtypes)
        print(data.columns)
